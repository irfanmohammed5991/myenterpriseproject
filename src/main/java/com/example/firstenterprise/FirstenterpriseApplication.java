package com.example.firstenterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class FirstenterpriseApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstenterpriseApplication.class, args);
	}

}
