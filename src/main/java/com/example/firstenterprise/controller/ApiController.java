package com.example.firstenterprise.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.firstenterprise.model.Message;
import com.example.firstenterprise.service.MessageService;



@RestController
@RequestMapping("/api/messages")
public class ApiController {
    private final MessageService messageService;

    @Autowired
    public ApiController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public List<Message> getAllMessages() {
        return messageService.getAllMessages();
    }

    @PostMapping
    public Message createMessage(@RequestBody Message message) {
        return messageService.create(message);
    }

    @PutMapping
    public Message updateMessage(@RequestBody Message message) {
        return messageService.update(message);
    } 

    @DeleteMapping
    public Message deleteMessage() {
        return null;
    }

}
