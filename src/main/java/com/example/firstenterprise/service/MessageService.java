package com.example.firstenterprise.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.firstenterprise.model.Message;
import com.example.firstenterprise.repository.MessageRepository;

@Service
public class MessageService {

    @Autowired
    private  MessageRepository messageRepository;

    public List<Message> getAllMessages() {
        return messageRepository.findAll();
    }

    public Message create(Message message) {
        return messageRepository.save(message);
    }

    public Message update(Message message) {
        return messageRepository.save(message);
    }
}
