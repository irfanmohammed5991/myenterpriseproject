FROM openjdk
ADD target/firstenterprise-0.0.1-SNAPSHOT.jar firstenterprise.jar
CMD ["java", "-jar", "firstenterprise.jar"]
